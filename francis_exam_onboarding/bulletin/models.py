# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save

# Create your models here.


class Board(models.Model):
    name = models.CharField(max_length=200)
    creator = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    created_on = models.DateTimeField(auto_now_add=True)
    position = models.PositiveIntegerField(default=0)

    def get_recent_post(self):
        posts = self.post_set.all().order_by("-posted_on")
        if posts.count() > 0:
            return posts[0]
        else:
            return None


class Thread(models.Model):
    title = models.CharField(max_length=200)
    board = models.ForeignKey(Board, on_delete=models.DO_NOTHING)
    posted_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    posted_on = models.DateTimeField(auto_now_add=True)
    is_locked = models.BooleanField(default=False)

    def get_recent_post(self):
        posts = self.post_set.all().order_by("-posted_on")
        if posts.count() > 0:
            return posts[0]
        else:
            return None
    def get_recent_poster_profile_id(self):
        posts = self.post_set.all().order_by("-posted_on")
        if posts.count() > 0:
            return posts[0].posted_by.userprofile.id
        else:
            return None


class Post(models.Model):
    message = models.CharField(max_length=10000)
    thread = models.ForeignKey(Thread, on_delete=models.DO_NOTHING)
    board = models.ForeignKey(Board, on_delete=models.DO_NOTHING)
    posted_on = models.DateTimeField(auto_now_add=True)
    posted_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_banned = models.BooleanField(default=False)


def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])


post_save.connect(create_profile, sender=User)
