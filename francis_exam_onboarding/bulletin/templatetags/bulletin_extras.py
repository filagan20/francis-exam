from django import template
from django.contrib.auth.models import Group

register = template.Library()

def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return group in user.groups.all()

@register.filter(name='is_admin')
def is_admin(user):
    return has_group(user, "Administrator")

@register.filter(name='is_mod')
def is_mod(user):
    return has_group(user, "Moderator")