from django.conf.urls import url, include

from django.contrib.auth import views as auth_views

from . import views as bulletin_views

app_name = 'bulletin'

board_patterns = [
    url(r'^create$', bulletin_views.CreateBoard.as_view(), name='create_board'),
    url(r'^(?P<pk>[0-9]+)$', bulletin_views.board_first_page, name='view_board'),
    url(r'^(?P<pk>[0-9]+)/page-(?P<page>[0-9]+)/$', bulletin_views.ViewBoard.as_view(), name='view_board_page'),
    url(r'^(?P<pk>[0-9]+)/edit/$', bulletin_views.EditBoard.as_view(), name='edit_board'),
    url(r'^delete/$', bulletin_views.DeleteBoard.as_view() , name='delete_board'),
]

thread_patterns = [
    url(r'^create/(?P<pk>[0-9]+)$', bulletin_views.CreateThread.as_view(), name='create_thread'),
    url(r'^(?P<pk>[0-9]+)/$', bulletin_views.thread_first_page, name='view_thread'),
    url(r'^(?P<pk>[0-9]+)/page-(?P<page>[0-9]+)/$', bulletin_views.ViewThread.as_view(), name='view_thread_page'),
    url(r'^(?P<pk>[0-9]+)/edit/$', bulletin_views.EditThread.as_view(), name='edit_thread'),
    url(r'^(?P<pk>[0-9]+)/create-post/$', bulletin_views.create_post, name='create_post'),
]

urlpatterns = [
    url(r'^$', bulletin_views.index_first_page , name='index'),
    url(r'^page-(?P<page>[0-9]+)/$', bulletin_views.IndexView.as_view() , name='view_index_page'),
    url(r'^login/$', auth_views.login, {'template_name':'bulletin/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name':'bulletin/logout.html'}, name='logout'),
    url(r'^register/$', bulletin_views.register , name='register'),
    url(r'^board/', include(board_patterns)),
    url(r'^thread/', include(thread_patterns)),
    url(r'^profiles/(?P<pk>[0-9]+)/$', bulletin_views.profile_first_page, name='view_profile'),
    url(r'^profiles/(?P<pk>[0-9]+)/page-(?P<page>[0-9]+)$', bulletin_views.ViewUserProfile.as_view(), name='view_profile_page'),
    url(r'^profiles/(?P<pk>[0-9]+)/set-ban/(?P<is_ban>[\w]+)$', bulletin_views.set_user_ban, name='set_user_ban'),
]