# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.paginator import Paginator

from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from .templatetags import bulletin_extras


from .forms import RegistrationForm, BoardForm, ThreadForm, PostForm

from .models import Board, Thread, Post, UserProfile


# Create your views here.
class UserPermissionsMixin(object):
    """Mixin to provide access to Users Permissions"""
    user = None

    @property
    def is_admin(self):
        if self.user != None:
            return bulletin_extras.is_admin(self.user)
        else:
            return False

    @property
    def is_mod(self):
        if self.user != None:
            return bulletin_extras.is_mod(self.user)
        else:
            return False

    @property
    def is_banned(self):
        if self.user != None:
            return self.user.userprofile.is_banned
        else:
            return False


class PaginationMixin(object):
    main_list = []
    number_of_contents_per_list = 20

    @property
    def get_paginator(self):
        return Paginator(self.main_list, self.number_of_contents_per_list)


class BaseView(LoginRequiredMixin, UserPermissionsMixin, generic.View):
    login_url = '/bulletin/login/'
    redirect_field_name = 'redirect_to'


class IndexView(PaginationMixin, BaseView):
    def get(self, request, *args, **kwargs):
        self.user = request.user
        page_number = self.kwargs['page']
        self.main_list = Board.objects.all().order_by('position')
        list_paginator = self.get_paginator
        page_count = list_paginator.num_pages
        if int(page_number) > page_count:
            return redirect(reverse("bulletin:view_index_page", args=(page_count,)))
        args = {'is_banned': self.is_banned,
                'page_object': list_paginator.page(page_number),
                'page_count': page_count,
                'page_number': int(page_number), }
        return render(request, 'bulletin/home.html', args)


class CreateBoard(BaseView):
    def get(self, request, *args, **kwargs):
        self.user = request.user
        if self.is_admin and not self.is_banned:
            form = BoardForm()
            form.creator = request.user
            args = {'form': form}
            return render(request, 'bulletin/create_board.html', args)
        else:
            return redirect(reverse("bulletin:index"))

    def post(self, request, *args, **kwargs):
        form = BoardForm(request.POST)
        if form.is_valid():
            new_board = form.save(commit=False)
            new_board.creator = request.user
            new_board.save()
            return redirect(reverse("bulletin:view_board", args=(new_board.pk,)))
        else:
            args = {'form': form}
            return render(request, 'bulletin/create_board.html', args)


class EditBoard(BaseView):
    def get(self, request, *args, **kwargs):
        self.user = request.user
        pk = self.kwargs['pk']
        if self.is_admin and not self.is_banned:
            board = get_object_or_404(Board, pk=pk)
            form = BoardForm(instance=board)
            args = {'form': form}
            return render(request, 'bulletin/edit_board.html', args)
        else:
            return redirect(reverse("bulletin:view_board", args=(pk,)))

    def post(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        board = get_object_or_404(Board, pk=pk)
        form = BoardForm(request.POST, instance=board)
        if form.is_valid():
            form.save()
            return redirect(reverse('bulletin:view_board', args=(pk,)))
        else:
            args = {'form': form}
            return render(request, 'bulletin/edit_board.html', args)


class ViewBoard(PaginationMixin, BaseView):
    def get(self, request, *args, **kwargs):
        self.user = request.user
        pk = self.kwargs['pk']
        page_number = self.kwargs['page']
        board = get_object_or_404(Board, pk=pk)
        self.main_list = board.thread_set.all().order_by("-posted_on")
        list_paginator = self.get_paginator
        page_count = list_paginator.num_pages
        if int(page_number) > page_count:
            return redirect(reverse("bulletin:view_board_page", args=(pk, page_count,)))
        args = {'board': board, 'is_banned': self.is_banned,
                'page_object': list_paginator.page(page_number),
                'page_count': list_paginator.num_pages,
                'page_number': int(page_number)}
        return render(request, 'bulletin/view_board.html', args)


class DeleteBoard(BaseView):
    def post(self, request, *args, **kwargs):
        self.user = request.user
        pk = request.POST['delete_board_id']
        if self.is_admin and not self.is_banned:
            Board.objects.filter(pk=pk).delete()
        return redirect(reverse("bulletin:index"))


class CreateThread(BaseView):
    def get(self, request, *args, **kwargs):
        form = ThreadForm()
        args = {'form': form}
        return render(request, 'bulletin/create_thread.html', args)

    def post(self, request, *args, **kwargs):
        form = ThreadForm(request.POST)
        pk = self.kwargs['pk']
        board = get_object_or_404(Board, pk=pk)
        if form.is_valid():
            new_thread = form.save(commit=False)
            new_thread.board = board
            new_thread.posted_by = request.user
            new_thread.save()
            return redirect(reverse("bulletin:view_thread", args=(new_thread.pk,)))
        else:
            args = {'form': form}
            return render(request, 'bulletin/create_thread.html', args)


class EditThread(BaseView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        thread = get_object_or_404(Thread, pk=pk)
        form = ThreadForm(instance=thread)
        args = {'form': form}
        return render(request, 'bulletin/edit_thread.html', args)

    def post(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        thread = get_object_or_404(Thread, pk=pk)
        form = ThreadForm(request.POST, instance=thread)
        if form.is_valid():
            form.save()
            return redirect(reverse("bulletin:view_thread", args=(pk,)))


class ViewThread(PaginationMixin, BaseView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        page_number = self.kwargs['page']
        thread = get_object_or_404(Thread, pk=pk)
        self.main_list = thread.post_set.all().order_by("posted_on")
        list_paginator = self.get_paginator
        page_count = list_paginator.num_pages
        if int(page_number) > page_count:
            return redirect(reverse("bulletin:view_thread_page", args=(pk, page_count,)))
        user = request.user
        form = PostForm()
        args = {'thread': thread, 'form': form, 'user': user,
                'page_object': list_paginator.page(page_number),
                'page_count': list_paginator.num_pages,
                'page_number': int(page_number)
                }
        return render(request, 'bulletin/view_thread.html', args)


class ViewUserProfile(PaginationMixin, BaseView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        page_number = self.kwargs['page']
        self.user = request.user
        is_self = request.user.userprofile.pk == int(pk)
        user_profile = get_object_or_404(UserProfile, pk=pk)
        self.main_list = user_profile.user.post_set.all().order_by("-posted_on")
        list_paginator = self.get_paginator
        page_count = list_paginator.num_pages
        if int(page_number) > page_count:
            return redirect(reverse("bulletin:view_profile_page", args=(pk, page_count,)))
        args = {'user_profile': user_profile, 'is_self': is_self, 'page_object': list_paginator.page(page_number),
                'page_count': list_paginator.num_pages,
                'page_number': int(page_number)
                }
        return render(request, 'bulletin/view_user_profile.html', args)


def register(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('bulletin:index'))
        else:
            args = {'form': form}
            return render(request, 'bulletin/register.html', args)
    else:
        form = RegistrationForm()
        args = {'form': form}
        return render(request, 'bulletin/register.html', args)


def create_post(request, pk):
    thread = get_object_or_404(Thread, pk=pk)
    form = PostForm(request.POST)
    if form.is_valid():
        new_post = form.save(commit=False)
        new_post.thread = thread
        new_post.board = thread.board
        new_post.posted_by = request.user
        new_post.save()
    return redirect(reverse("bulletin:view_thread", args=(thread.pk,)))


def set_user_ban(request, pk, is_ban):
    user_profile = get_object_or_404(UserProfile, pk=pk)
    if is_ban == "true":
        user_profile.is_banned = True
    else:
        user_profile.is_banned = False
    user_profile.save()
    return redirect(reverse("bulletin:view_profile", args=(pk,)))


def thread_first_page(request, pk):
    page = 1
    return redirect(reverse("bulletin:view_thread_page", args=(pk, page,)))


def board_first_page(request, pk):
    page = 1
    return redirect(reverse("bulletin:view_board_page", args=(pk, page,)))


def index_first_page(request):
    page = 1
    return redirect(reverse("bulletin:view_index_page", args=(page,)))


def profile_first_page(request, pk):
    page = 1
    return redirect(reverse("bulletin:view_profile_page", args=(pk, page,)))
