# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Board, Thread, Post, UserProfile

class ThreadInline(admin.TabularInline):
    model = Thread
    fields = ('title', 'posted_by', 'is_locked')

class PostInline(admin.TabularInline):
    model = Post
    fields = ('message', 'posted_by', 'posted_on')

class PostInline(admin.TabularInline):
    model = Post

class BoardAdmin(admin.ModelAdmin):
    list_display = ('name', 'creator', 'created_on')
    inlines = [ThreadInline,]

class ThreadAdmin(admin.ModelAdmin):
    list_display = ('title', 'posted_by', 'posted_on', 'is_locked')
    inlines = [PostInline,]

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user',)


# Register your models here.
admin.site.register(Board, BoardAdmin)
admin.site.register(Thread, ThreadAdmin)
admin.site.register(Post)
admin.site.register(UserProfile, UserProfileAdmin)