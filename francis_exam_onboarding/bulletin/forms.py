from django import forms
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import (
    UserCreationForm,
    UserChangeForm,
    PasswordChangeForm,
)

from .models import Board, Thread, Post, UserProfile


class RegistrationForm(UserCreationForm):
    groups = forms.ModelChoiceField(queryset=Group.objects.all())

    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
            'groups',
        )

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.is_active = True

        if commit:
            user.save()
            user_group = self.cleaned_data['groups']
            user_group.user_set.add(user)


        return user


class BoardForm(forms.ModelForm):
    class Meta:
        model = Board
        fields = ['name', 'position']


class ThreadForm(forms.ModelForm):
    class Meta:
        model = Thread
        fields = ['title', 'is_locked']


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['message']
        widgets = {
            'message': forms.Textarea(attrs={'rows': 3, 'style': 'width:100%'})
        }
